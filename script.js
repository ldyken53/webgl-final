function makeSet(value) {
    const singleton = {
        rank: 0,
        value: value
    };
    singleton.parent = singleton;

    return singleton;
};

function find(node) {
    if (node.parent !== node) {
        node.parent = find(node.parent);
    }

    return node.parent;
};

function union(node1, node2) {
    const root1 = find(node1);
    const root2 = find(node2);
    if (root1 !== root2) {
        if (root1.rank < root2.rank) {
            root1.parent = root2;
        } else {
            root2.parent = root1;
            if (root1.rank === root2.rank) root1.rank += 1;
        }
    }
};

function makeLowest(node1) {
    var root = find(node1);
    node1.rank += node1.parent.rank;
    node1.parent = node1;
    root.parent = node1;
}

function equals(node1, node2) {
    const root1 = find(node1);
    const root2 = find(node2);
    if (root1 == root2) {
        return true
    } else {
        return false
    }
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {

        // Generate random number  
        var j = Math.floor(Math.random() * (i + 1));

        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }

    return array;
}

function extractJoinTree() {
    document.getElementById('stopButton').onclick = stop;
    document.getElementById('startButton').onclick = start;
    //just assume (in 2-dimensional data set) adjacent entries are up down left right and diagonals
    const testArray = [
        29, 37, 39, 70, 74, 84, 38, 36, 26,
        27, 100, 49, 72, 85, 89, 83, 28, 24,
        25, 47, 50, 73, 86, 90, 71, 82, 22,
        23, 75, 79, 48, 69, 87, 88, 81, 18,
        19, 76, 80, 78, 46, 68, 67, 40, 16,
        17, 41, 77, 45, 35, 20, 21, 32, 15,
        13, 42, 43, 44, 34, 33, 31, 30, 14,
        12, 11, 10, 9, 8, 7, 6, 5, 0
    ]
    const width = 9;
    const height = 8;
    jt = [];
    //var dataSet = [];
    var i;
    var comps = [];
    var nodes = [];
    var dataMapping = {};
    for (i = 0; i < width * height; i++) {
        //dataSet.push(i);
        nodes.push({ id: i });
        comps[i] = makeSet(i);
        if (dataMapping[testArray[i]]) {
            dataMapping[testArray[i]].push(i)
        } else {
            dataMapping[testArray[i]] = [i];
        }
    }
    //shuffleArray(dataSet);
    sortedDescSet = testArray.slice().sort((a, b) => b - a);
    //console.log(dataSet, sortedDescSet);
    gData = { nodes: nodes, links: jt };
    Graph = ForceGraph()
        (document.getElementById('graph'))
        .graphData(gData);
    let x = 0;
    startLoop();
    function startLoop() {
        var refreshID = setInterval(function timer() {
            const { nodes, links } = Graph.graphData();
            var jt = [];
            //this is slow and will run into problems in data sets with duplicates
            //to fix this, need to set up a dictionary when constructing our sorted set
            i = dataMapping[sortedDescSet[x]].shift();
            neighbors = [i - width - 1, i - width, i - 1, i + 1, i + width, i + width + 1];
            neighbors.forEach(neighborIndex => {
                if (testArray[neighborIndex] >= sortedDescSet[x]) {
                    lowestVertex = find(comps[neighborIndex]);
                    jt.push({ source: lowestVertex.value, target: i });
                    union(comps[neighborIndex], comps[i]);
                }
            })
            makeLowest(comps[i]);
            Graph.graphData({
                nodes: nodes,
                links: [...links, ...jt]
            });
            x++;
            if (stopper == 0) {
                console.log('stop');
                clearInterval(refreshID);
            }
        }, 1000);
    }

    function stop() {
        stopper = 0;
    }

    function start() {
        stopper = 1;
        startLoop();
    }
}

function extractUnaugmentedContourTree(data) {
    const length = Math.sqrt(data.length);
    //var dataSet = [];
    var i;
    var comps = [];
    var dataMapping = {};
    for (i = 0; i < data.length; i++) {
        //dataSet.push(i);
        comps[i] = null;
        if (dataMapping[data[i]]) {
            dataMapping[data[i]].push(i)
        } else {
            dataMapping[data[i]] = [i];
        }
    }
    sortedDescSet = data.slice().sort((a, b) => b - a);
    joinEdge = {}
    joinSuperArc = {}
    var x = 0;
    while (x < sortedDescSet.length) {
        i = dataMapping[sortedDescSet[x]].shift();
        nNeighboringComponents = 0;
        // 3D
        // neighbors = [i - 1, i + 1, i - length, i + length, i + width * length, i - width + length];
        // 2D
        neighbors = [i - 1, i + 1, i - length, i + length];
        neighbors.forEach(neighborIndex => {
            if (comps[neighborIndex] == null) {

            } else if (nNeighboringComponents == 0) {
                comps[i] = makeSet(i);
                union(comps[neighborIndex], comps[i]);
                nNeighboringComponents++;
            } else if (!equals(comps[neighborIndex], comps[i])) {
                joinEdge[find(comps[neighborIndex]).value][1] = i;
                edge = joinEdge[find(comps[i]).value]
                union(comps[neighborIndex], comps[i]);
                if (nNeighboringComponents == 1) {
                    edge[1] = i;
                    makeLowest(comps[i]);
                    joinEdge[find(comps[i]).value] = [i, null];
                }
                nNeighboringComponents++;
            }
        })
        if (nNeighboringComponents == 0) {
            comps[i] = makeSet(i);
            joinEdge[find(comps[i]).value] = [i, null];
        }
        joinSuperArc[i] = joinEdge[find(comps[i]).value];
        x++;
    }
    Object.values(joinEdge).forEach(value => {
        if (!value[1]) {
            value[1] = i;
            joinEdge[i] = null;
        }
    });
    console.log(joinEdge);
    // var i;
    // var comps = [];
    // var dataMapping = {};
    // for (i = 0; i < width * height; i++) {
    //     //dataSet.push(i);
    //     comps[i] = null;
    //     if (dataMapping[testArray[i]]) {
    //         dataMapping[testArray[i]].push(i)
    //     } else {
    //         dataMapping[testArray[i]] = [i];
    //     }
    // }
    // sortedAscSet = testArray.slice().sort((a, b) => a - b);
    // splitEdge = {}
    // splitSuperArc = {}
    // x = 0;
    // while (x < sortedAscSet.length) {
    //     i = dataMapping[sortedAscSet[x]].shift();
    //     nNeighboringComponents = 0;
    //     neighbors = [i - width - 1, i - width, i - 1, i + 1, i + width, i + width + 1];
    //     neighbors.forEach(neighborIndex => {
    //         if (comps[neighborIndex] == null) {

    //         } else if (nNeighboringComponents == 0) {
    //             comps[i] = makeSet(i);
    //             union(comps[neighborIndex], comps[i]);
    //             nNeighboringComponents++;
    //         } else if (!equals(comps[neighborIndex], comps[i])) {
    //             splitEdge[find(comps[neighborIndex]).value][1] = i;
    //             edge = splitEdge[find(comps[i]).value]
    //             union(comps[neighborIndex], comps[i]);
    //             if (nNeighboringComponents == 1) {
    //                 edge[1] = i;
    //                 makeLowest(comps[i]);
    //                 splitEdge[find(comps[i]).value] = [i, null];
    //             }
    //             nNeighboringComponents++;
    //         }
    //     })
    //     if (nNeighboringComponents == 0) {
    //         comps[i] = makeSet(i);
    //         splitEdge[find(comps[i]).value] = [i, null];
    //     }
    //     splitSuperArc[i] = splitEdge[find(comps[i]).value];
    //     x++;
    // }
    // Object.values(splitEdge).forEach(value => {
    //     if (!value[1]) {
    //         value[1] = i;
    //         splitEdge[i] = null;
    //     }
    // });
    // Object.keys(splitEdge).sort((a, b) => {
    //     return testArray[b] - testArray[a];
    // }).forEach(key => {
    //     if (!(key in joinEdge)) {
    //         key = parseInt(key);
    //         var edge = joinSuperArc[key];
    //         joinEdge[key] = [edge[0], key];
    //         edge[0] = key;
    //     }
    // });
    // Object.keys(joinEdge).sort((a, b) => {
    //     return testArray[a] - testArray[b];
    // }).forEach(key => {
    //     if (!(key in splitEdge)) {
    //         key = parseInt(key);
    //         var edge = splitSuperArc[key];
    //         splitEdge[key] = [edge[0], key];
    //         edge[0] = key;
    //     }
    // });
    var nodes = [];
    var links = [];
    joinEdgeValues = {}
    Object.keys(joinEdge).forEach(key => {
        nodes.push({ id: parseInt(key) });
        if (joinEdge[key]) {
            links.push({ source: joinEdge[key][0], target: joinEdge[key][1] });
        }
    });
    var gdata = { nodes: nodes, links: links };
    // splitEdgeValues = {}
    // Object.keys(splitEdge).forEach(key => {
    //     if (splitEdge[key]) {
    //         splitEdgeValues[testArray[key]] = [testArray[splitEdge[key][0]], testArray[splitEdge[key][1]]];
    //     }
    // });
    // console.log("splitEdgevalues", splitEdgeValues);
    // console.log(splitEdge);
    console.log(nodes);
    const Graph = ForceGraph()
        (document.getElementById('graph'))
        .dagMode('td')
        .graphData(gdata)
        .nodeId('id')
        .nodeAutoColorBy('group')
        .nodeCanvasObject((node, ctx, globalScale) => {
            const label = data[node.id].toFixed(3);
            const fontSize = 12 / globalScale;
            ctx.font = `${fontSize}px Sans-Serif`;
            const textWidth = ctx.measureText(label).width;
            const bckgDimensions = [textWidth, fontSize].map(n => n + fontSize * 0.2); // some padding

            ctx.fillStyle = 'rgba(255, 255, 255, 0.8)';
            ctx.fillRect(node.x - bckgDimensions[0] / 2, node.y - bckgDimensions[1] / 2, ...bckgDimensions);

            ctx.textAlign = 'center';
            ctx.textBaseline = 'middle';
            ctx.fillStyle = node.color;
            ctx.fillText(label, node.x, node.y);
        });
}

function loadData() {
    var fileInput = document.getElementById("myfileinput");
    var files = fileInput.files;
    var reader = new FileReader();

    reader.onload = function () {
        var data = new Float32Array(reader.result);
        extractUnaugmentedContourTree(data);
        var processed_data = Float32Array();
        var min = Math.min(...data);
        var max = Math.max(...data);
        data.forEach(i => {
            processed_data.push(((i-min)/(max-min))*179)
        });

    }

    reader.readAsArrayBuffer(files[0]);
}

function setup() {
    var input = document.getElementById("myfileinput");
    input.onchange = loadData;
}

function extractSplitTree() {
    document.getElementById('stopButton').onclick = stop;
    document.getElementById('startButton').onclick = start;
    //just assume (in 2-dimensional data set) adjacent entries are up down left right and diagonals
    const testArray = [
        29, 37, 39, 70, 74, 84, 38, 36, 26,
        27, 100, 49, 72, 85, 89, 83, 28, 24,
        25, 47, 50, 73, 86, 90, 71, 82, 22,
        23, 75, 79, 48, 69, 87, 88, 81, 18,
        19, 76, 80, 78, 46, 68, 67, 40, 16,
        17, 41, 77, 45, 35, 20, 21, 32, 15,
        13, 42, 43, 44, 34, 33, 31, 30, 14,
        12, 11, 10, 9, 8, 7, 6, 5, 0
    ]
    const width = 9;
    const height = 8;
    jt = [];
    //var dataSet = [];
    var i;
    var comps = [];
    var nodes = [];
    var dataMapping = {};
    for (i = 0; i < width * height; i++) {
        //dataSet.push(i);
        nodes.push({ id: i });
        comps[i] = makeSet(i);
        if (dataMapping[testArray[i]]) {
            dataMapping[testArray[i]].push(i)
        } else {
            dataMapping[testArray[i]] = [i];
        }
    }
    //shuffleArray(dataSet);
    sortedAscSet = testArray.slice().sort((a, b) => a - b);
    //console.log(dataSet, sortedDescSet);
    gData = { nodes: nodes, links: jt };
    Graph = ForceGraph()
        (document.getElementById('graph'))
        .graphData(gData);
    let x = 0;
    startLoop();
    function startLoop() {
        var refreshID = setInterval(function timer() {
            const { nodes, links } = Graph.graphData();
            var jt = [];
            //this is slow and will run into problems in data sets with duplicates
            //to fix this, need to set up a dictionary when constructing our sorted set
            i = dataMapping[sortedAscSet[x]].shift();
            neighbors = [i - width - 1, i - width, i - 1, i + 1, i + width, i + width + 1];
            neighbors.forEach(neighborIndex => {
                if (testArray[neighborIndex] <= sortedAscSet[x]) {
                    lowestVertex = find(comps[neighborIndex]);
                    jt.push({ source: lowestVertex.value, target: i });
                    union(comps[neighborIndex], comps[i]);
                }
            })
            makeLowest(comps[i]);
            Graph.graphData({
                nodes: nodes,
                links: [...links, ...jt]
            });
            x++;
            if (stopper == 0) {
                console.log('stop');
                clearInterval(refreshID);
            }
        }, 1000);
    }

    function stop() {
        stopper = 0;
    }

    function start() {
        stopper = 1;
        startLoop();
    }
}

var stopper = 1;
window.onload = setup;